provider "google" {
  project = "libreml"
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_compute_instance" "vm_instance" {
  name         = "gitlab-bastion"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
      size = 30
    }
  }

  network_interface {
    network       = "default"
    access_config {
    }
  }

  metadata = {
   sshKeys = "libreml:${file("~/.ssh/id_rsa.pub")}"
  }

  service_account {
    scopes = ["cloud-platform"]
  }
}

output "gcp_external_ip" {
  value = "${google_compute_instance.vm_instance.network_interface.0.access_config.0.nat_ip}"
}

resource "google_compute_network" "vpc_network" {
  name                    = "gitlab-bastion-network"
  auto_create_subnetworks = "true"
}
