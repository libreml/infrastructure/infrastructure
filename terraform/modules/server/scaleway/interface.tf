variable "type" {
  description = "The machine spec"
  default     = "ARM64-16GB"
}

variable "count" {
  description = "Number of servers to create"
  default     = 1
}
