variable "do_token" {}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_volume" "cache_volume" {
  region      = "lon1"
  name        = "volume-lon1"
  size        = 100
  description = "Volume to store ostree repo(s)"
}

resource "digitalocean_droplet" "cache" {
  count = 1

  image  = "fedora-27-x64"
  name   = "artifacts-cache"
  region = "lon1"
  size   = "s-1vcpu-1gb"
  private_networking = "true"

  volume_ids = ["${digitalocean_volume.cache_volume.id}"]
}

resource "digitalocean_floating_ip" "cache_floating_ip" {
  droplet_id = "${digitalocean_droplet.cache.id}"
  region     = "${digitalocean_droplet.cache.region}"
}

output "cache_floating_ip" {
  value = "${digitalocean_floating_ip.cache_floating_ip.ip_address}"
}
