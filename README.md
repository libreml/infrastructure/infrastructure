# Libreml infrastructure

This is a fork of https://gitlab.com/freedesktop-sdk/infrastructure/infrastructure

This repo holds:
- Terraform instructions to provision the required infrastructure
- Ansible playbooks for setting up services in that infrastructure

## libreml artifact server

This is buildstreams included cache, which is an implementation of the CAS standard. We run a seperate server
which hosts all of the artifacts, which have been build/stored from the master branch.

Currently we have 1 server, configured using ansible.

- libreml-cache.libreml.org

The infrastructure for the cache server is created with terraform. This creates a digitalocean droplet and connects 
a volume with it. The current configuration of this can be found [here](/terraform/cache_server/main.tf#L11).

To use the terrform configuration you will require
  - A digitalocean account
  - An API token for the digitalocean account.

```
    # Going to the appropiate folder
    cd terraform/cache-server

    # Initialize terraform
    terraform init

    # Check changes
    terraform plan

    # Apply changes
    terraform apply
```

Please note that the volume must be formatted manually to `ext4` before the ansible script can be used. This can be done via
ssh access to the droplet.

```
sudo mkfs.ext4 /dev/disk/by-id/scsi-0DO_Volume_<VOLUME_NAME>
```

It can then be configured using Ansible as follows:
```
  ansible-playbook --ask-vault-pass -i hosts ./artifacts/playbook.yml -e 'ansible_python_interpreter=/usr/bin/python3'
```

Users will only have the ability to pull from the cache server. Access for pushing is restricted to libreml maintainers.

## SSL certificates
The certificates are automatically generated via the Dehydrated client/nginx configuration documented in
the Ansible scripts.

You need to ensure that the droplet floating IP is [mapped](https://www.digitalocean.com/docs/networking/dns/how-to/manage-records/) to the domain used. [Floating IPs](https://www.digitalocean.com/docs/networking/floating-ips/) ensure that droplets can be interchanged without downtime.

## Gitlab runner autoscaling

Libreml builds can take a long time to complete, and some jobs require more compute and/or storage requirements. Libreml infrastructure provides terraform and ansible instructions for setting up your own autoscaling gitlab runners. This is configured currently to use [Google Cloud Platform](https://cloud.google.com/) (GCP)

Currently we have one bastion server that sets up GCP compute instances that carry out gitlab jobs. We utilise preemptable instances to save on costs and runner characteristics are highly [user configurable](ansible/roles/gitlab-runner/defaults/main.yml) from the ansible configuration.

To set up gitlab-runner autoscaling you will require the following:
  - The runner registration token for the gitlab project
  - A GCP account (Note that the free trial has tight restrictions on the number of vCPUs allowed)
  - API access + service account key for the GCP account.
  - Your own SSH key to access the bastion server.

To provision a new bastion server using terraform, you need to set the `GOOGLE_CLOUD_KEYFILE_JSON` environment variable with a [service
account key](https://cloud.google.com/iam/docs/creating-managing-service-account-keys)

Once this is done, you can provision a bastion server via:

```
    # Going to the appropiate folder
    cd terraform/gitlab-bastion

    # Initialize terraform
    terraform init

    # Check changes
    terraform plan

    # Apply changes
    terraform apply
```

This uses a `f1-micro` instance with a Debian Stretch image, which is eligible for the free tier. The ssh key that is used to access the server is located at the default [`~/.ssh/id_rsa.pub`](terraform/gitlab-bastion/main.tf#L25), with user `libreml`.

If the terraform is successful, the external IP of the server will be presented. Accessing the server can be done by

```
ssh libreml@[IP of server]
```

To provision the server with ansible, you will need to modify the [hosts file](ansible/hosts#L4) with the IP of the bastion server. Once this is done, the server can be provisioned via:

```
cd ansible
ansible-playbook -i hosts gitlab-runner.yml -e 'gitlab_token=[RUNNER_REGISTRATION_TOKEN]'
```
